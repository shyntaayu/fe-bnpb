import { TestBed } from '@angular/core/testing';

import { KejadianService } from './kejadian.service';

describe('KejadianService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KejadianService = TestBed.get(KejadianService);
    expect(service).toBeTruthy();
  });
});
