import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogistikModalComponent } from './logistik-modal.component';

describe('LogistikModalComponent', () => {
  let component: LogistikModalComponent;
  let fixture: ComponentFixture<LogistikModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogistikModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogistikModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
