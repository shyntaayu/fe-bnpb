import { Component, OnInit, EventEmitter, Output, ViewChild, ElementRef, Injector } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'applogistikmodal',
  templateUrl: './logistik-modal.component.html',
  styleUrls: ['./logistik-modal.component.css']
})
export class LogistikModalComponent implements OnInit {

  legalForm: FormGroup;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('modal') modal: ModalDirective;
  @ViewChild('nameInput') nameInput: ElementRef;

  constructor(
    injector: Injector,
    private _fb: FormBuilder,
  ) { 
    this.legalForm = _fb.group({
      'jenisLogistik': [null, Validators.required],
      'jumlah': [null, Validators.required],
      'provinsi': [null, Validators.required],
      'kotaKab': [null, Validators.required],
      'keterangan': [null, Validators.required],
    })
  }

  ngOnInit() {
  }

  show() {
    this.modal.show();
  }

  close() {
    // this.reset();
    // this.active = false;
    this.modal.hide();
    // this.modalHide.emit();
  }

  onShown(): void {
    // if (this.model.payTypeCode) {
    //     $(this.name.nativeElement).focus();
    // } else {
    //     $(this.code.nativeElement).focus();
    // }
}

}
