import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class KejadianService {

  private urlDetail = "http://alfath.tech/bnpb/api/kejadian/detail/310020170101/";
  private urlKejadian = "http://alfath.tech/bnpb/api/kejadian/";

  constructor(
    private http: Http,
  ) { }
  
}
