import { Component, ViewChild } from '@angular/core';
import { _getViewData } from '@angular/core/src/render3/instructions';
import { Observable, of, } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Http, Response } from '@angular/http';
import { MapGeoJsonService } from './share/services/map-geo-json.service';
import { LatLngLiteral } from '@agm/core';
import { AgmSnazzyInfoWindow } from '@agm/snazzy-info-window';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'fe-bnpb';

  lat: number = -6.2312416035297815;
  lng: number = 106.83020622484533;
  zoom: number = 11;
  geoJsonObject: Object;
  polygon: Array<LatLngLiteral>;

  infoWindowLat: number;
  infoWindowLong: number;
  infoWindowIsOpen: boolean = false;
  infoWindowText: string;

  private urlDetail = "http://alfath.tech/bnpb/api/kejadian/detail/310020170101/";
  private urlKejadian = "http://alfath.tech/bnpb/api/kejadian/";

  @ViewChild("gm") map;

  constructor(
    private geoJsonService: MapGeoJsonService,
    private http: Http,
  ) { }

  ngOnInit() {
    this.getMap();
    this.getKejadian();
    this.getDetail();
  }

  getDetail() {
    this.http.get(this.urlDetail).subscribe((res: Response) => {
      let a = res.json()
      console.log(a);
    });
  }

  getKejadian() {
    this.http.get(this.urlKejadian).subscribe((res: Response) => {
      let a = res.json()
      console.log(a);
    });
  }

  getMap() {
    this.geoJsonService.getGeoJson()
      .subscribe(result => {
        this.geoJsonObject = result;
        // this.paths = result.features[0].geometry.geometries[1].coordinates[0];
        // console.log(result.features[0]);
        // this.polygon = [];
        // result.features[1].geometry.geometries[1].coordinates[0].forEach(coord => {
        //   console.log(typeof coord[1]);
        //   this.polygon.push({ "lat": parseFloat(coord[1].toString()), "lng": parseFloat(coord[0].toString()) });
        // });

        console.log(this.polygon);
        //vm.map.polygon = feature.geometry;

        // vm.map.polygon = [];
        // feature.geometry.coordinates.forEach(function (coord) {
        //   vm.map.polygon.push({ latitude: coord[1], longitude: coord[0] });
        // });
        //vm.map.polygon = feature.geometry;
      });

  }

  styleFunc(feature) {
    return ({
      fillColor: "green",
      strokeWeight: 2,
      clickable: true,
      label: feature.getProperty('label'),
      glyph: feature.getProperty('id'),
      // icon: feature.getProperty('icon')
    });
  }

  markerClick(clickEvent, gm, infoWindow) {
    console.log(clickEvent.feature)
    this.infoWindowText = clickEvent.feature.getProperty('description');
    var position = clickEvent.feature.getGeometry();
    this.infoWindowLat = clickEvent.latLng.lat();
    this.infoWindowLong = clickEvent.latLng.lng();
    this.infoWindowIsOpen = true;
    infoWindow.open();
    this.map.lastOpen = infoWindow;
  }

  onMouseOver(clickEvent, infoWindow, gm) {
    console.log(infoWindow)
    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }
    gm.lastOpen = infoWindow;
    // this.infoWindowText = "AAAAA";
    // this.infoWindowLat = gm.latitude;
    // this.infoWindowLong = gm.longitude;
    this.infoWindowIsOpen = true;
    infoWindow.open();
  }


  polyClicked(index: number, polygon, infoWindow,gm) {
    console.log(polygon) // this works correctly

    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }
    gm.lastOpen = infoWindow;
    this.infoWindowText = "AAAAA";
    this.infoWindowLat = polygon[0].lat;
    this.infoWindowLong = polygon[0].lng;
    // this.infoWindowIsOpen = true;
    this.infoWindowIsOpen = true
    infoWindow.open()
    // }
  }
  
}
