import { Component, OnInit, Injector, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'appfilterkejadian',
  templateUrl: './filter-kejadian.component.html',
  styleUrls: ['./filter-kejadian.component.css']
})
export class FilterKejadianComponent implements OnInit {

  legalForm: FormGroup;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('modal') modal: ModalDirective;
  @ViewChild('nameInput') nameInput: ElementRef;

  constructor(
    injector: Injector,
    private _fb: FormBuilder,
  ) { 
    this.legalForm = _fb.group({
      'tahun': [null, Validators.required],
      'bulan': [null, Validators.required],
      'provinsi': [null, Validators.required],
      'kotaKab': [null, Validators.required],
      'bencana': [null, Validators.required],
    })
  }

  ngOnInit() {
  }

  show() {
    this.modal.show();
  }

  close() {
    // this.reset();
    // this.active = false;
    this.modal.hide();
    // this.modalHide.emit();
  }

  onShown(): void {
    // if (this.model.payTypeCode) {
    //     $(this.name.nativeElement).focus();
    // } else {
    //     $(this.code.nativeElement).focus();
    // }
}

}
