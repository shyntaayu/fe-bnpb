import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterKejadianComponent } from './filter-kejadian.component';

describe('FilterKejadianComponent', () => {
  let component: FilterKejadianComponent;
  let fixture: ComponentFixture<FilterKejadianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterKejadianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterKejadianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
