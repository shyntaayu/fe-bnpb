import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AgmCoreModule } from '@agm/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmSnazzyInfoWindowModule } from '@agm/snazzy-info-window';
import { HttpModule, JsonpModule } from '@angular/http';
import { ModalModule, TooltipModule, TabsModule } from 'ngx-bootstrap';
import { KejadianModalComponent } from './kejadian-modal/kejadian-modal.component';
import { ShareModule } from './share/share.module';
import { DataTableModule, DropdownModule, PaginatorModule, ButtonModule, SpinnerModule, EditorModule, CheckboxModule, RadioButtonModule } from 'primeng/primeng';
import { UtilsModule } from './share/utils/utils.module';
import { KorbanModalComponent } from './korban-modal/korban-modal.component';
import { LogistikModalComponent } from './logistik-modal/logistik-modal.component';
import { FilterKejadianComponent } from './filter-kejadian/filter-kejadian.component';

@NgModule({
  declarations: [
    AppComponent,
    KejadianModalComponent,
    KorbanModalComponent,
    LogistikModalComponent,
    FilterKejadianComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAfK2s8rs13YTTuc055PdoJjBYvd1vDywU'
    }),
    AgmSnazzyInfoWindowModule,
    HttpModule,
    JsonpModule,
    ModalModule.forRoot(),
    ShareModule,
    ReactiveFormsModule,
    ButtonModule,
    UtilsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
