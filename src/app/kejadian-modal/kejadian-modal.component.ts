import { Component, OnInit, Injector, EventEmitter, ElementRef, ViewChild, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'appkejadianmodal',
  templateUrl: './kejadian-modal.component.html',
  styleUrls: ['./kejadian-modal.component.css']
})
export class KejadianModalComponent implements OnInit {

  legalForm: FormGroup;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('modal') modal: ModalDirective;
  @ViewChild('nameInput') nameInput: ElementRef;


  constructor(
    injector: Injector,
    private _fb: FormBuilder,
  ) {
    this.legalForm = _fb.group({
      // 'docNo': [null, Validators.required],
      'jenisBencana': [null, Validators.required],
      'tanggalKejadian': [null, Validators.required],
      'provinsi': [null, Validators.required],
      'kotaKab': [null, Validators.required],
      'sebab': [null, Validators.required],
      'deskripsi': [null, Validators.required],
    })
  }

  ngOnInit() {
  }

  show() {
    this.modal.show();
  }

  onShown(): void {
    // if (this.model.payTypeCode) {
    //     $(this.name.nativeElement).focus();
    // } else {
    //     $(this.code.nativeElement).focus();
    // }
  }

  close(param): void {
    let self = this;
    if (param == "a") {
      // this.message.confirm(
      //   this.l("Are you sure to cancel? Your data will lost."),
      //   function (isConfirmed) {
      //     if (isConfirmed) {
      //       self.closeAll();
      //     }
      //   }
      // );
    } else {
      self.closeAll();
    }
  }

  closeAll() {
    this.reset();
    // this.active = false;
    this.modal.hide();
    // this.modalHide.emit();
  }

  reset() {
  }

}
