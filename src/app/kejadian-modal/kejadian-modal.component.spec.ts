import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KejadianModalComponent } from './kejadian-modal.component';

describe('KejadianModalComponent', () => {
  let component: KejadianModalComponent;
  let fixture: ComponentFixture<KejadianModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KejadianModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KejadianModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
