import { Injector } from '@angular/core';
import { Moment } from 'moment';
import * as moment from 'moment';
import { PrimengDatatableHelper } from './helper/PrimengDatatableHelper';

export class ModalProperty {
    edit: boolean
    id: number
    name: string
}

export abstract class AppComponentBase {

    primengDatatableHelper: PrimengDatatableHelper;

    constructor(injector: Injector) {
        this.primengDatatableHelper = new PrimengDatatableHelper();
    }

    //== CUSTOM FUNC =====================================
    isEmptyValue(n): boolean {
        if (n === undefined || n === null || n === '' || n === 0)
            return true;
        else
            return false;
    }

    daysBetween = function (startDate, endDate) {
        // Convert both dates to milliseconds
        var startDate_ms = startDate.getTime();
        var endDate_ms = endDate.getTime();

        // Calculate the difference in milliseconds
        var difference_ms = endDate_ms - startDate_ms;
        //take out milliseconds
        difference_ms = difference_ms / 1000;
        var seconds = Math.floor(difference_ms % 60);
        difference_ms = difference_ms / 60;
        var minutes = Math.floor(difference_ms % 60);
        difference_ms = difference_ms / 60;
        var hours = Math.floor(difference_ms % 24);
        var days = Math.floor(difference_ms / 24);

        var diff = '';
        if (days != 0) diff += days + ' days, ';
        if (hours != 0) diff += hours + ' hours, ';
        if (minutes != 0) diff += minutes + ' minutes, ';

        diff += seconds + ' seconds ';

        return diff;
    }

    //NOTE: This method only cover date with / or - separator
    strToDate(date: string, format: string = 'dd/mm/yyyy', isDatetime: boolean = false): Date {
        let separator = date.indexOf('-') != -1 ? '-' : '/';
        let splitDate: string[];
        let formattedDate: Date;
        let splitTime: string = '';

        if(isDatetime) {
            let idxSpace = date.indexOf(' ')
            splitDate = date.substr(0, idxSpace).split(separator);
            splitTime = date.substr(idxSpace);
        } else {
            splitDate = date.split(separator);
        }
        
        if(format=='dd'+separator+'mm'+separator+'yyyy')
            formattedDate = new Date(splitDate[2]+'-'+splitDate[1]+'-'+splitDate[0]+splitTime);
        else if(format=='mm'+separator+'dd'+separator+'yyyy')
            formattedDate = new Date(splitDate[2]+'-'+splitDate[0]+'-'+splitDate[1]+splitTime);
        else
            formattedDate = new Date(splitDate[0]+'-'+splitDate[1]+'-'+splitDate[2]+splitTime);

        return formattedDate;
    }

    //NOTE: This method only cover date with / or - separator
    strToMoment(date: string, format: string = 'dd/mm/yyyy', isDatetime: boolean = false): Moment {
        let dateInput = moment(this.strToDate(date, format, isDatetime));
        return dateInput;
    }

    momentToStr(date: Moment): string {
        if(date){
            return date.format('DD/MM/YYYY');
        }
        else{
            return null;
        }
    }

    momentToDateTimeStr(date: Moment): string {
        return date.format('DD/MM/YYYY hh:mm:ss A');
    }

    roundTo(n, digits) {
        if (digits === undefined) {
            digits = 0;
        }

        let multiplicator = Math.pow(10, digits);
        n = parseFloat((n * multiplicator).toFixed(11));
        let test = (Math.round(n) / multiplicator);
        return +(test.toFixed(digits));
    }

    currencyFormattedNumber(number, thousandsSeparator = ',', decimalSeparator = '.'): string {
        let parts = number.toString().split('.');
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousandsSeparator);
        return parts.join(decimalSeparator) + (parts.length === 1 ? decimalSeparator + '00' : (parts[1].length === 1 ? '0' : ''));
    }
}
