import { AfterViewInit, Component, ElementRef, forwardRef, Injector, OnInit, ViewChild, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Dropdown } from 'primeng/primeng';
import { AppComponentBase } from './app-component-base';

const noop = () => {
};
export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => KotaDDLComponent),
    multi: true
};

@Component({
    selector: 'kota-ddl',
    template: `
    <div>
        <select #kotaDropdown
        [disabled]="isDisabled"
            class="form-control"
            [attr.data-live-search]="true"
            [(ngModel)]="inputValue"
            (blur)="onBlur()">
                <option *ngFor="let a of project" [value]="a.value">{{a.label}}</option>
        </select>
    </div>`,
    providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class KotaDDLComponent extends AppComponentBase implements ControlValueAccessor, OnInit, AfterViewInit {
    @ViewChild('kotaDropdown') dropdown: ElementRef;
    @Input() isDisabled = false;
    @Input('input') input: number;


    //The internal data model
    private innerValue: any = '';
    project: any[] = [];
    isLoading = false;

    //Placeholders for the callbacks which are later provided
    //by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    constructor(injector: Injector) {
        super(injector);
    }

    //get accessor
    get inputValue(): any {
        return this.innerValue;
    }

    ngOnInit(): void {
        this.isLoading = true;
        this.project = [
            { value: "", label: 'Nothing Selected' }, {
                value: 1,
                label: "JakPus",
            },
            {
                value: 2,
                label: "Malang",
            },
        ]
        this.isLoading = false;
    }

    refreshAll() {
        setTimeout(() => {
            $(this.dropdown.nativeElement).selectpicker('refresh');
        }, 0);
    }

    //set accessor including call the onchange callback
    set inputValue(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
            this.refreshAll();
        }
    }

    ngAfterViewInit(): void {
        $(this.dropdown.nativeElement).selectpicker({
            iconBase: 'famfamfam-flag',
            tickIcon: 'fa fa-check'
        });
    }

    //Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    //From ControlValueAccessor interface
    writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
            this.refreshAll();
        }
    }

    //From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    ngOnChanges(): void {
        // this.inputValue(this.innerValue);
        this.refreshAll();
    }

    //From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }
}

