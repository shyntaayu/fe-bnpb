import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule, TabsModule, TooltipModule } from 'ngx-bootstrap';
import { InputMaskDirective } from "./input-mask.directive";
import { PriceFormatDirective } from "./price-format.directive";
import { TrimValueAccessor } from "./trim-value-accessor.directive";
import { FormControlStyleComponent } from "./form-control-style.component";
import { ControlMessageComponent } from "./control-message.component";
import { BaseDropDownListComponent } from "./base-ddl.component";
import { UtilsModule } from "./utils/utils.module";
import { ProvinsiDDLComponent } from "./provinsi-ddl.component";
import { KotaDDLComponent } from "./kota-ddl.component";
import { JenisBencanaDDLComponent } from "./jenis-bencana-ddl.component";
import { JenisLogistikDDLComponent } from "./jenis-logistik-ddl.component";
import { KecamatanDDLComponent } from "./kecamatan-ddl.component";
import { StatusDDLComponent } from "./status-ddl.component";
import { BulanDDLComponent } from "./bulan-ddl.component";
import { TahunDDLComponent } from "./tahun-ddl.component";
import { BencanaDDLComponent } from "./bencana-ddl.component";

@NgModule({
        imports: [
                CommonModule,
                FormsModule, ReactiveFormsModule,
                ModalModule, TabsModule, TooltipModule,
                UtilsModule,
        ],
        declarations: [
                InputMaskDirective,
                PriceFormatDirective,
                TrimValueAccessor,

                FormControlStyleComponent,
                ControlMessageComponent,
                ProvinsiDDLComponent,
                KotaDDLComponent,
                JenisBencanaDDLComponent,
                JenisLogistikDDLComponent,
                KecamatanDDLComponent,
                StatusDDLComponent,
                TahunDDLComponent,
                BulanDDLComponent,
                BencanaDDLComponent,
        ],
        exports: [
                InputMaskDirective,
                PriceFormatDirective,
                TrimValueAccessor,
                FormControlStyleComponent,
                ControlMessageComponent,
                ProvinsiDDLComponent,
                KotaDDLComponent,
                JenisBencanaDDLComponent,
                JenisLogistikDDLComponent,
                KecamatanDDLComponent,
                StatusDDLComponent,
                TahunDDLComponent,
                BulanDDLComponent,
                BencanaDDLComponent,
        ]
})

export class ShareModule { }
