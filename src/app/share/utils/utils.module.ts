import { NgModule } from '@angular/core';

import { EqualValidator } from './validation/equal-validator.directive';
import { PasswordComplexityValidator } from './validation/password-complexity-validator.directive';
import { MinValueValidator } from './validation/min-value-validator.directive';
import { ButtonBusyDirective } from './button-busy.directive';
import { AutoFocusDirective } from './auto-focus.directive';
import { LocalStorageService } from './local-storage.service';
import { MomentFormatPipe } from './moment-format.pipe';
import { CurrencyInputDirective } from './currency-input.directive';
import { NormalizeDropdownPositionDirective } from './normalize-dropdown-position.directive';
import { DatePickerDirective } from './date-picker.component';


@NgModule({
    providers: [
        LocalStorageService
    ],
    declarations: [
        EqualValidator,
        PasswordComplexityValidator,
        MinValueValidator,
        ButtonBusyDirective,
        AutoFocusDirective,
        MomentFormatPipe,
        CurrencyInputDirective,
        NormalizeDropdownPositionDirective,
        DatePickerDirective,
    ],
    exports: [
        EqualValidator,
        PasswordComplexityValidator,
        MinValueValidator,
        ButtonBusyDirective,
        AutoFocusDirective,
        MomentFormatPipe,
        CurrencyInputDirective,
        NormalizeDropdownPositionDirective,
        DatePickerDirective,
    ]
})
export class UtilsModule { }
