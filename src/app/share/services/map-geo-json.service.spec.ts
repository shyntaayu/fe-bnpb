import { TestBed } from '@angular/core/testing';

import { MapGeoJsonService } from './map-geo-json.service';

describe('MapGeoJsonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MapGeoJsonService = TestBed.get(MapGeoJsonService);
    expect(service).toBeTruthy();
  });
});
