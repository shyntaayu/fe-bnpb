import { Component, OnInit, ElementRef, ViewChild, Output, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'appkorbanmodal',
  templateUrl: './korban-modal.component.html',
  styleUrls: ['./korban-modal.component.css']
})
export class KorbanModalComponent implements OnInit {

  legalForm: FormGroup;

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('modal') modal: ModalDirective;
  @ViewChild('nameInput') nameInput: ElementRef;
  
  constructor(
    injector: Injector,
    private _fb: FormBuilder,
  ) { 
    this.legalForm = _fb.group({
      'status': [null, Validators.required],
      'usia': [null, Validators.required],
      'ahliWaris': [null, Validators.required],
      'kotaKab': [null, Validators.required],
      'kecamatan': [null, Validators.required],
      'keterangan': [null, Validators.required],
    })
  }

  ngOnInit() {
  }

  show() {
    this.modal.show();
  }

  close() {
    // this.reset();
    // this.active = false;
    this.modal.hide();
    // this.modalHide.emit();
  }

  onShown(): void {
    // if (this.model.payTypeCode) {
    //     $(this.name.nativeElement).focus();
    // } else {
    //     $(this.code.nativeElement).focus();
    // }
}

}
