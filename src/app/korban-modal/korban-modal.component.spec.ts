import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KorbanModalComponent } from './korban-modal.component';

describe('KorbanModalComponent', () => {
  let component: KorbanModalComponent;
  let fixture: ComponentFixture<KorbanModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KorbanModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KorbanModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
